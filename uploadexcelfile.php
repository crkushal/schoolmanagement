<?php
	include('connection.php');
	include('userdata.php');
	$classid = $_GET['id'];
	if (isset($_POST['submit'])) {
		
			$myData="";
			$flag = true;
			$filename = $_FILES['add-excel']['tmp_name'];
	 		$filetempname = $_FILES['add-excel']['tmp_name'];
	 		$handel = fopen($filetempname,"r");
	 		if ($filename == NULL) {
	 			echo "file is empty";
	 		}	
	 		else{
	 			while (($myData = fgetcsv($handel,1000,",")) !== false) {
		 			if ($flag) {
		 				$flag = false;
		 				continue;
		 			}
		 			$name = $myData[0];
		 			$roll = $myData[1];
			 		$address = $myData[2];		
			 		$sql = new Insertdata();
			 		$data = $sql->excelData($name,$roll,$address,$classid);
			 		if ($data) {
			 			header("location:eachclass.php?id=".$classid);
			 		}
			 		else{
			 			header("location:uploadexcelfile.php?id=".$classid);
			 				
		 			}	
		 		}
	 		}
		
		
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>classes</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content animated fadeIn">
				<div class="heading">
					Add Student Data
				</div>
				<div class="register-form">
					<form method="POST" action="" enctype="multipart/form-data">
						<div class="form-group">
						    <label for="exampleInputEmail1">Name:</label>
						    <input type="file" name="add-excel" class="form-control" id="excelfile" >
						</div>
					  	<button type="submit" name="submit" class="btn btn-primary" id="button">Upload</button>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>