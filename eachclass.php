<?php include('navbarafter.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>each class students</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css"
</head>
<body>
	
	<div class="container">
		<?php
			include('userdata.php');
			$classid = $_GET['id'];
			$select = new Selectdata();
			$each_class_student = $select->selectEachClass($classid); 
			$row = mysqli_num_rows($each_class_student);
			if ($row > 0) {
				$sql = new Selectdata();
				$class = $sql->selectClass();
				$row2 = mysqli_fetch_array($class);
				// session_start();
				$session = $_SESSION['user']['role_id'];
				if ($session == 100) {
					echo "<table class='table table-striped table-bordered table-background'>
					<tr class='each-heading'>
						<th colspan='5'>
							<div class='eachclass-heading'>Student Of Class:".$row2['class_name']."</div>
						</th>
						
					</tr>
					<tr>
						<th>ID</th>
						<th>ROLLNO</th>
						<th>NAME</th>
						<th>ADDRESS</th>
						<th>ACTIONS</th>
					</tr>";
								
					while($row1 = mysqli_fetch_array($each_class_student))
					{
						echo "<tr>";
							echo "<td>" . $row1['student_id'] . "</td>";
							echo "<td>" . $row1['student_roll'] . "</td>";
							echo "<td>" . $row1['student_name'] . "</td>";
							echo "<td>" . $row1['student_address'] . "</td>";
							echo "<td>
							<a href='eachstudent.php?id=".$row1['student_id']."'>EachStudent</a>
							</td>";
						echo "</tr>";
					}				
				echo "</table>";
				}
				if ($session == 50) {
					echo "<table class='table table-striped table-bordered table-background'>
					<tr class='each-heading'>
						<th colspan='5'>
							<div class='eachclass-heading'>Student Of Class:".$row2['class_name']."</div>
						</th>
						
					</tr>
					<tr>
						<th>ID</th>
						<th>ROLLNO</th>
						<th>NAME</th>
						<th>ADDRESS</th>
						<th>ACTIONS</th>
					</tr>";
								
					while($row1 = mysqli_fetch_array($each_class_student))
					{
						echo "<tr>";
							echo "<td>" . $row1['student_id'] . "</td>";
							echo "<td>" . $row1['student_roll'] . "</td>";
							echo "<td>" . $row1['student_name'] . "</td>";
							echo "<td>" . $row1['student_address'] . "</td>";
							echo "<td>
							<a href='delete.php?id=".$row1['student_id']."'><i class='fa fa-trash'></i></a>
							<a href='updatestudentform.php?id=".$row1['student_id']."&class_id=".$row1['class_id']."'><i class='fa fa-pencil'></i></a>
							<a href='eachstudent.php?id=".$row1['student_id']."'>EachStudent</a>
							</td>";
						echo "</tr>";
					}				
				echo "</table>";
				}
				if ($session == 20) {
					echo "<table class='table table-striped table-bordered table-background'>
					<tr class='each-heading'>
						<th colspan='5'>
							<div class='eachclass-heading'>Student Of Class:".$row2['class_name']."</div>
						</th>
						
					</tr>
					<tr>
						<th>ID</th>
						<th>ROLLNO</th>
						<th>NAME</th>
						<th>ADDRESS</th>
						<th>ACTIONS</th>
					</tr>";
								
					while($row1 = mysqli_fetch_array($each_class_student))
					{
						echo "<tr>";
							echo "<td>" . $row1['student_id'] . "</td>";
							echo "<td>" . $row1['student_name'] . "</td>";
							echo "<td>" . $row1['student_roll'] . "</td>";
							echo "<td>" . $row1['student_address'] . "</td>";
							echo "<td>
							<a href='updatestudentform.php?id=".$row1['student_id']."'><i class='fa fa-pencil'></i></a>
							</td>";
						echo "</tr>";
					}				
				echo "</table>";
				}
			}
			else{
				echo "no data";
			}
		 ?>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>