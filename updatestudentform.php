<?php
	include('navbarafter.php');
	if(isset($_SESSION['user']) || !empty($_SESSION['user'])){
	  	$session = $_SESSION['user']['role_id'];
	  	if ($session == 100) {
	  		header("location:home.php");
	  	}
	}
?>
<?php
	include('userdata.php');
	$studentid = $_GET['id'];

	if (isset($_POST['submit'])) {
	 	// if (!empty($_POST['update-name']) && !empty($_POST['update-roll']) && !empty($_POST['update-address'])) {
			$classid = $_GET['class_id'];
	 		$update_name = $_POST['update-name'];
	 		$update_roll = $_POST['update-roll'];
	 		$update_address = $_POST['update-address'];
	 		$update = new Update();
	 		$update_student = $update->updateStudent($studentid,$update_name,$update_roll,$update_address,$classid);
	 		if ($update_student) {
	 			header("location:eachclass.php?id=".$classid);
	 		}
	 		else{
	 			header("location:updatestudentform.php?id=".$studentid);
	 		}
	 		
	 	// }
	 } 
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>classes</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content animated fadeIn">
				<div class="heading">
					Update Student
				</div>
				<?php 
					$select = new Selectdata();
					$sel = $select->selectStudent($studentid);
					$row = mysqli_fetch_array($sel);
				 ?>
				<div class="register-form">
					<form method="POST" action="">
						<div class="form-group">
						    <label for="exampleInputEmail1">Name:</label>
						    <input type="text" name="update-name" class="form-control" id="r-username" value="<?php echo $row['student_name'];?>">
						</div>
						<div class="form-group">
						    <label for="exampleInputEmail1">Roll No:</label>
						    <input type="text" name="update-roll" class="form-control" id="r-username" value="<?php echo $row['student_roll'];?>">
						</div>
						<div class="form-group">
						    <label for="exampleInputEmail1">Address:</label>
						    <input type="text" name="update-address" class="form-control" id="r-username" value="<?php echo $row['student_address'];?>">
						</div>
					  	<button type="submit" name="submit" class="btn btn-primary" onclick="register()" id="button">Submit</button>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>