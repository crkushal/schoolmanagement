<?php
	include('userdata.php');
	$roleid = 100;
	$select = new Selectdata();
	$select_user = $select->selectUsers($roleid);
	$row = mysqli_num_rows($select_user);
	if ($row > 0) {
		header("location:loginform.php");
	}
	else{
		if (isset($_POST['submit'])) {
			if (!empty($_POST['register-name']) && !empty($_POST['register-username']) && !empty($_POST['register-password'])) {
				$name = $_POST['register-name'];
				$username = $_POST['register-username'];
				$password = $_POST['register-password'];
				$insert = new Insertdata();
				$insert_user = $insert->registerUser($name,$username,$password,$roleid);
				if ($insert_user) {
					header('location:loginform.php');
				}
				else{
					header("location:registerform.php");
				}
			}
		}
		
	}
	
 ?>