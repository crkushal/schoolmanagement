<?php 
	include('navbarafter.php');
	if(!isset($_SESSION['user']) || empty($_SESSION['user'])){
	  	header("location:loginform.php");
	}
	else{
		header("home.php");
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>classes</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body>
	<div class="container">
		<?php
			$session = $_SESSION['user']['role_id'];
			include('userdata.php');
			$select = new Selectdata();
			$class_select = $select->selectClass();
			$class = mysqli_num_rows($class_select);
			if ($class > 0) {
				if ($session == 100) {
					echo "<table class='table table-striped table-bordered classdetail-table table-background animated fadeIn'>
						<tr class='each-heading'>
							<th colspan='3'>Classes</th>
							</tr>
							<tr class='classdetail-heading'>
								<th>CLASSID</th>
								<th>CLASS</th>
							</tr>";
						while($row = mysqli_fetch_array($class_select))
						{
							echo "<tr>";
								echo "<td>" . $row['class_id'] . "</td>";
								echo "<td><a href='eachclass.php?id=".$row['class_id']."'>" . $row['class_name'] . "</a></td>";
							echo "</tr>";
						}
					echo "</table>";
				}
				if ($session == 50 ) {
					echo "<table class='table table-striped table-bordered classdetail-table table-background animated fadeIn'>
					<tr class='each-heading'>
						<th colspan='3'>Classes</th>
						</tr>
						<tr class='classdetail-heading'>
							<th>CLASSID</th>
							<th>CLASS</th>
							<th>Actions</th>
						</tr>";
					while($row = mysqli_fetch_array($class_select))
					{
						echo "<tr>";
							echo "<td>" . $row['class_id'] . "</td>";
							echo "<td><a href='eachclass.php?id=".$row['class_id']."'>" . $row['class_name'] . "</a></td>";
							echo "<td>
							<a href='updateclassform.php?id=".$row['class_id']."'>update</a>
							<a href='deleteclass.php?id=".$row['class_id']."'>delete</i></a>
							<a href='addstudentform.php?id=".$row['class_id']."'>Add Students</a></td>";
						echo "</tr>";
					}
					echo "</table>";
				}
				if ($session == 20) {
					echo "<table class='table table-striped table-bordered classdetail-table table-background animated fadeIn'>
					<tr class='each-heading'>
						<th colspan='3'>Classes</th>
						</tr>
						<tr class='classdetail-heading'>
							<th>CLASSID</th>
							<th>CLASS</th>
							
						</tr>";
					while($row = mysqli_fetch_array($class_select))
					{
						echo "<tr>";
							echo "<td>" . $row['class_id'] . "</td>";
							echo "<td><a href='eachclass.php?id=".$row['class_id']."'>" . $row['class_name'] . "</a></td>";
						echo "</tr>";
					}
					echo "</table>";
				}
			}
			else{
				echo "no class data found";
			}

		 ?>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>