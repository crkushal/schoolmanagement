<!DOCTYPE html>
<html>
<head>
	<title>classes</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body>
	<?php include('navbarafter.php'); ?>
	<div class="container">
		<?php
			include('userdata.php');
			$select = new Selectdata();
			$user_select = $select->selectUser();
			$user = mysqli_num_rows($user_select);
			if ($user > 0) {
				if ($session == 100) {
					echo "<table class='table table-striped table-bordered classdetail-table table-background animated fadeIn'>
						<tr class='classdetail-heading'>
							<th colspan='6'>Users</th>
							</tr>
							<tr class='classdetail-heading'>
								<th>UserId:</th>
								<th>Name</th>
								<th>Username</th>
								<th>Role</th>
							</tr>";
						while($row = mysqli_fetch_array($user_select))
						{
							echo "<tr>";
								echo "<td>" . $row['user_id'] . "</td>";
								echo "<td>" . $row['user_name'] . "</td>";
								echo "<td>" . $row['user_username'] . "</td>";
								// echo "<td>" . $row['user_password'] . "</td>";
								echo "<td>" ;
									if ($row['role_id'] == 100) {
										echo "Principle";
									}
									if ($row['role_id'] == 50) {
										echo "Administrator";
									}
									if ($row['role_id'] == 20) {
										echo "Teacher";
									}
								"</td>";
							echo "</tr>";
						}
					echo "</table>";
				}
				if ($session == 50) {
					echo "<table class='table table-striped table-bordered classdetail-table table-background animated fadeIn'>
						<tr class='classdetail-heading'>
							<th colspan='6'>Users</th>
							</tr>
							<tr class='classdetail-heading'>
								<th>UserId:</th>
								<th>Name</th>
								<th>Username</th>
								
								<th>Role</th>
								<th>Actions</th>
							</tr>";
						while($row = mysqli_fetch_array($user_select))
						{
							echo "<tr>";
								echo "<td>" . $row['user_id'] . "</td>";
								echo "<td>" . $row['user_name'] . "</td>";
								echo "<td>" . $row['user_username'] . "</td>";
								// echo "<td>" . $row['user_password'] . "</td>";
								echo "<td>" ;
									if ($row['role_id'] == 100) {
										echo "Principle";
									}
									if ($row['role_id'] == 50) {
										echo "Administrator";
									}
									if ($row['role_id'] == 20) {
										echo "Teacher";
									}
								"</td>";
								echo "<td>" ;
									if ($row['role_id'] == 100) {
										echo "denied";
									}
									if ($row['role_id'] == 50) {
										echo "<a href='updateuser.php?id=".$row['user_id']."'>update</a>
								<a href='deleteuser.php?id=".$row['user_id']."'>delete</i></a>";
									}
									if ($row['role_id'] == 20) {
										echo "<a href='updateuser.php?id=".$row['user_id']."'>update</a>
								<a href='deleteuser.php?id=".$row['user_id']."'>delete</i></a>";
									}
								"</td>";
							echo "</tr>";
						}
					echo "</table>";
				}
				if ($session == 20) {
					header("location:home.php");
				}
			}
			else{
				echo "no class data found";
			}

		 ?>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>