<?php
	include('userdata.php');
	include('navbarafter.php');
	if(isset($_SESSION['user']) || !empty($_SESSION['user'])){
	  	$session = $_SESSION['user']['role_id'];
	  	if ($session == 100) {
	  		header("location:home.php");
	  	}
	  	
	  	if ($session == 20) {
	  		header("location:home.php");
	  	}
	}
?>
<?php
$userid = $_GET['id'];
 	if (isset($_POST['submit'])) {
 		// if (!empty($_POST['update-name']) && !empty($_POST['update-username']) && !empty($_POST['update-password']) && !empty($_POST['update-role'])) {
 			$update_name = $_POST['update-name'];
 			$update_username = $_POST['update-username'];
 			$update_password = $_POST['update-password'];
 			$update_role = $_POST['update-roleid'];
 			$sql = new Update();
 			$update = $sql->updateUser($userid,$update_name,$update_username,$update_password,$update_role);
 			if ($update) {
 				header("location:users.php");
 			}
 			else{
 				header("location:updateuser.php?id=".$id);
 			}
 		// }
 	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body class="register-body">
	<?php 
		$select = new Selectdata();
		$sel = $select->selectUserInfo($userid);
		$row = mysqli_fetch_array($sel);
	?>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content animated fadeIn">
				<div class="heading">
					Update User
				</div>
				<div class="register-form">
					<form method="POST" action="">
						<div class="form-group">
							<i class="fa fa-user" aria-hidden="true"></i>
						    <label for="exampleInputEmail1">Name</label>
						    <input type="text" name="update-name" class="form-control" id="r-name" value="<?php echo $row['user_name'];?>">
						</div>
						<div class="form-group">
							<i class="fa fa-user" aria-hidden="true"></i>
						    <label for="exampleInputEmail1">Username</label>
						    <input type="text" name="update-username" class="form-control" id="r-username" value="<?php echo $row['user_username'];?>">
						</div>
					  	<div class="form-group">
						  	<i class="fa fa-key" aria-hidden="true"></i>
						    <label for="exampleInputPassword1">Password</label>
						    <input type="password" name="update-password" class="form-control" id="r-password" value="<?php echo $row['user_password'];?>">
					  	</div>
					  	<div class="form-group">
						  	<select name='update-roleid' class="form-control" >
						  		<option class="form-control" value='50'>Administrator</option>
						  		<option class="form-control" value='20'>Teacher</option>
						  	</select>
					  	</div>
					  	<button type="submit" name="submit" class="btn btn-primary" onclick="register()" id="button">Submit</button>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>