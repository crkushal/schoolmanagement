<?php
	include('navbarafter.php');
	if(isset($_SESSION['user']) || !empty($_SESSION['user'])){
	  	$session = $_SESSION['user']['role_id'];
	  	if ($session == 100) {
	  		header("location:home.php");
	  	}
	  	
	  	if ($session == 20) {
	  		header("location:home.php");
	  	}
	}
?>

<?php
	
	include('userdata.php');
	if (isset($_POST['add-class'])) {
	 	if (!empty($_POST['add-class'])) {
	 		$add_class = $_POST['add-class'];
	 		$checking = new Selectdata();
	 		$check = $checking->checkClass($add_class);
	 		$row = mysqli_fetch_array($check);
	 		if ($row > 0) {
	 			echo "this class already exists";
	 		}
	 		else{
	 			$add = new Insertdata();
	 			$class = $add->createClass($add_class);
	 			if ($class) {
	 				header("location:home.php");
	 			}
	 			else{
	 				header("location:addclassform.php");
	 			}
	 		}
	 	}
	 } 
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>classes</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content animated fadeIn">
				<div class="heading">
					Add Class
				</div>
				<div class="register-form">
					<form method="POST" action="">
						<div class="form-group">
						    <label for="exampleInputEmail1">Class</label>
						    <input type="text" name="add-class" class="form-control" id="r-username">
						</div>
					  	<button type="submit" name="submit" class="btn btn-primary" onclick="register()" id="button">Submit</button>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>