<!DOCTYPE html>
<html>
<head>
	<title>nav</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<style type="text/css">
		.container-fluid{
			margin:0;
			padding: 0;
		}
	</style>
</head>
<body>
	<?php 
		session_start();
		$session = $_SESSION['user']['role_id'];
		if ($session == 100) {
			$user = "Principle";
		}
		if ($session == 50) {
			$user = "Administrator";
		}
		if ($session == 20) {
			$user = "Teacher";
		}
	 ?>
	<div class="container-fluid">
		<div class="nav-before">
			<nav class="navbar navbar-expand-lg navbar-light bg-light custom-navbar">
			  <a class="navbar-brand" href="#">SM</a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>
			  <?php
			  	if ($session == 100) {
			  		?>
			  	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
					    <ul class="navbar-nav mr-auto">
					      <li class="nav-item">
					        <a class="nav-link" href="users.php">Users</a>
					      </li>
					       <li class="nav-item">
					        <a class="nav-link" href="adduser.php">Add User</a>
					      </li>
					      <li class="nav-item">
					        <a class="nav-link" href="home.php">Class</a>
					      </li>
					         <li class="nav-item">
					        <a class="nav-link" href="logout.php">Logout</a>
					      </li>

					    </ul>
					    <p><?php echo $user; ?></p>
					</div>
					<?php
			  	 } 
			  	 if ($session == 50) {
			  	 	?>
			  	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
					    <ul class="navbar-nav mr-auto">
					      <li class="nav-item">
					        <a class="nav-link" href="addclassform.php">Add Class</a>
					      </li>
					      <li class="nav-item">
					        <a class="nav-link" href="adduser.php">Add User</a>
					      </li>
					      <li class="nav-item">
					        <a class="nav-link" href="users.php">Users</a>
					      </li>
					      </li>
					       <li class="nav-item">
					        <a class="nav-link" href="home.php">Class</a>
					      </li>
					         <li class="nav-item">
					        <a class="nav-link" href="logout.php">Logout</a>
					      </li>
					    </ul>
					     <p><?php echo $user; ?></p>
					</div>
				<?php
			  	 }
			  	 if($session == 20)
			  	 {
			  	 	?>
			  	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
					    <ul class="navbar-nav mr-auto">
					      <li class="nav-item">
					        <a class="nav-link" href="addclassform.php">Add Class</a>
					      </li>
					      </li>
					       <li class="nav-item">
					        <a class="nav-link" href="home.php">Class</a>
					      </li>
					         <li class="nav-item">
					        <a class="nav-link" href="logout.php">Logout</a>
					      </li>
					      
					    </ul>
					     <p><?php echo $user; ?></p>
					</div>
					<?php
			  	 }
			   ?>

			  
			</nav>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>