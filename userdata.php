<?php
	include_once('connection.php');
	class Insertdata{
		private $conn;
		public function __construct(){
			$connection = new Connection();
			$this->conn = $connection->connect();
		}
		public function registerUser($name,$username,$password,$roleid){
			$sql = "INSERT INTO users(user_name,user_username,user_password,role_id) VALUES ('$name','$username','$password','$roleid')";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function createClass($add_class){
			$sql = "INSERT INTO classes(class_name) VALUES ('$add_class')";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function addStudent($name,$roll,$address,$classid){
			$sql = "INSERT INTO students(student_name,student_roll,student_address, class_id) VALUES ('$name','$roll','$address','$classid')";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function addUser($name,$username,$password,$roleid){
			$sql = "INSERT INTO users(user_name,user_username,user_password,role_id) VALUES ('$name','$username','$password',$roleid)";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function excelData($first_name,$last_name,$address,$classid){
			$sql = "INSERT INTO students (student_name,student_roll,student_address,class_id) VALUES ('$first_name','$last_name','$address','$classid')";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
	} 
	class Selectdata{
		private $conn;
		public function __construct(){
			$connection = new Connection();
			$this->conn = $connection->connect();
		}
		public function selectUsers($roleid){
			$sql = "SELECT * FROM users WHERE role_id = '$roleid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;

		}
		public function selectUserInfo($userid){
			$sql = "SELECT * FROM users WHERE user_id = '$userid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;

		}
		public function loggedUser($username,$password){
			$sql = "SELECT * FROM users WHERE user_username='$username' AND user_password='$password'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function selectClass(){
			$sql = "SELECT * FROM classes";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function selectClassDetail($classid){
			$sql = "SELECT * FROM classes WHERE class_id = '$classid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function selectEveryClass($classid,$roll){
			$sql = "SELECT * FROM students WHERE class_id ='$classid' AND student_roll='$roll'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function selectEachClass($classid){
			$sql = "SELECT * FROM students WHERE class_id ='$classid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function selctSpecificClass($update_class){
			$sql = "SELECT * FROM classes WHERE class_name ='$update_class'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function selectStudent($studentid){
			$sql = "SELECT * FROM students WHERE student_id = '$studentid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function checkClass($add_class){
			$sql = "SELECT * FROM classes where class_name='$add_class'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function selectUser(){
			$sql = "SELECT * FROM users ";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function selectEachStudent($student_id){
			$sql = "SELECT * FROM students WHERE student_id = '$student_id' ";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		
		
	}
	class Update {
		private $conn;
		public function __construct(){
			$connection = new Connection();
			$this->conn = $connection->connect();
		}
		public function updateClass($classid,$class){
			$sql = "UPDATE classes SET class_name='$class' WHERE class_id='$classid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function updateUser($userid,$update_name,$update_username,$update_password,$update_role){
			$sql = "UPDATE users SET user_name='$update_name',user_username='$update_username',user_password='$update_password',role_id='$update_role' WHERE user_id ='$userid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function updateStudent($studentid,$update_name,$update_roll,$update_address,$classid){
			$sql = "UPDATE students SET student_name='$update_name',student_roll='$update_roll',student_address='$update_address',class_id='$classid' WHERE student_id ='$studentid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		
	}
	class Delete {
		private $conn;
		public function __construct(){
			$connection = new Connection();
			$this->conn = $connection->connect();
		}
		public function deleteClass($classid){
			$sql = "DELETE FROM classes WHERE class_id = '$classid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function deleteStudent($studentid){
			$sql = "DELETE FROM students WHERE student_id = '$studentid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function deleteStd($classid){
			$sql = "DELETE FROM students WHERE class_id = '$classid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function deleteUser($userid){
			$sql = "DELETE FROM users WHERE user_id = '$userid'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}

	}
 ?>