<?php include('navbarafter.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>classes</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div>
					<?php
						include('userdata.php');
						$student_id = $_GET['id'];
						$student = new Selectdata();
						$select = $student->selectEachStudent($student_id);
						$row = mysqli_num_rows($select);
						if ($row > 0) {
							while($row1 = mysqli_fetch_array($select))
								{?>
								<table class='table table-striped table-bordered table-background student-table'>
								<tr class='each-heading'>
								<th>Student Detail</th>
								</tr>
								<tr>
								<th>Name:<?php echo $row1['student_name']; ?> </th>	
								</tr>
								<tr>
								<th>Roll No: <?php echo $row1['student_roll']; ?> </th>
								</tr>
								<tr>
								<th>Address:  <?php echo $row1['student_address']; ?></th>
								</tr>
								<tr>
								<th>ClassId:  <?php 
									$classid = $row1['class_id'];
									$sql = new Selectdata();
									$query = $sql->selectClassDetail($classid);
									if ($query) {
										$row3 = mysqli_fetch_array($query);
										echo $row3['class_name'];
									}
								?></th>
								</tr>
										
								
								<?php	

								}	
								?>			
							</table>
							<?php
						}
						else{
							echo "nothing to display";
						} 

					 ?>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>