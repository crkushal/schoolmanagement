<?php
	include('navbarafter.php');
	if(isset($_SESSION['user']) || !empty($_SESSION['user'])){
	  	$session = $_SESSION['user']['role_id'];
	  	if ($session == 100) {
	  		header("location:home.php");
	  	}
	  	
	  	if ($session == 20) {
	  		header("location:home.php");
	  	}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>classes</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body>
	<?php
	include('userdata.php');
	$classid = $_GET['id'];
	if (isset($_POST['submit'])) {
	 	if (!empty($_POST['add-name']) && !empty($_POST['add-roll']) && !empty($_POST['add-address'])) {
	 		$name = $_POST['add-name'];
	 		$roll = $_POST['add-roll'];
	 		$address = $_POST['add-address'];
	 		
	 		$checking = new Selectdata();
	 		$check = $checking->selectEveryClass($classid,$roll);
	 		$row = mysqli_num_rows($check);
	 		if ($row > 0) {
	 			$roll_exist = "roll no already exists";
	 			echo $roll_exist;
	 		}
	 		else{
	 			$add = new Insertdata();
	 			$student = $add->addStudent($name,$roll,$address,$classid);
	 			
	 			if ($student) {
	 				header("location:eachclass.php?id=".$classid);
	 			}
	 			else{
	 				echo "error";
	 				header("location:addstudentform.php?id=".$classid);
	 			}
	 		}
	 	}
	 } 
 ?>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content animated fadeIn">
				<div class="heading">
					Add Student
				</div>
				<div class="register-form">
					<form method="POST" action="">
						<div class="form-group">
						    <label for="exampleInputEmail1">Name:</label>
						    <input type="text" name="add-name" class="form-control" id="r-username" >
						</div>
						<div class="form-group">
						    <label for="exampleInputEmail1">Roll No:</label>
						    <input type="text" name="add-roll" class="form-control" id="r-username" >
						</div>
						<div class="form-group">
						    <label for="exampleInputEmail1">Address:</label>
						    <input type="text" name="add-address" class="form-control" id="r-username">
						</div>
						<div class="form-group">
						    <a href=uploadexcelfile.php?id=<?php echo $classid;?>>Or Upload Excel File</a>
						</div>
					  	<button type="submit" name="submit" class="btn btn-primary" onclick="register()" id="button">Submit</button>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>