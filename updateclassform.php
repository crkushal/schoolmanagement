<?php
	include('navbarafter.php');
	if(isset($_SESSION['user']) || !empty($_SESSION['user'])){
	  	$session = $_SESSION['user']['role_id'];
	  	if ($session == 100) {
	  		header("location:home.php");
	  	}
	  	
	  	if ($session == 20) {
	  		header("location:home.php");
	  	}
	}
?>
<?php
	include('userdata.php');
	$classid = $_GET['id'];
	if (isset($_POST['update-class'])) {
	 	if (!empty($_POST['update-class'])) {
	 		
	 		$update_class = $_POST['update-class'];
	 		$class = new Selectdata();
	 		$class_check = $class->selctSpecificClass($update_class	);
	 		$row = mysqli_num_rows($class_check);
	 		if ($row > 0) {
	 			echo "class already exists";
	 		}
	 		else{
	 			$update = new Update();
		 		$update_class = $update->updateClass($classid,$update_class);
		 		if ($update_class) {
		 			header("location:home.php");
		 		}
		 		else{
		 			header("location:updateclassform.php");
		 		}
	 		}
	 	}
	 } 
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>classes</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content animated fadeIn">
				<div class="heading">
					Update Class
				</div>
				<?php
					$select = new Selectdata();
					$class = $select->selectClassDetail($classid);
					$row = mysqli_fetch_array($class); 
				 ?>
				<div class="register-form">
					<form method="POST" action="">
						<div class="form-group">
						    <label for="exampleInputEmail1">Update Class</label>
						    <input type="text" name="update-class" class="form-control" id="r-username" value="<?php echo $row['class_name'];?>">
						</div>
					  	<button type="submit" name="submit" class="btn btn-primary" onclick="register()" id="button">Submit</button>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script src="js/costum.js"></script>
</body>
</html>